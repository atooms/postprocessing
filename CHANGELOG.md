# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 4.0.2 - 2024/10/23

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/4.0.1...4.0.2)

###  Bug fixes
- Fix missing `norigins` argument in `NonGaussianParameter` - thanks to Saichao
- Fix `numpy.product` deprecation

## 4.0.0 - 2024/04/28

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.4.0...4.0.0)

###  Changed
- A few breaking changes to base `Correlation` class
  - Drop `Correlation.value_square`
  - Drop `Correlation.update()`
  - Drop `Correlation.comments`
  - Rename `Correlation.phasespace` to `Correlation.variables`
  - Refactor `Correlation` name handling, this will break custom `Correlation` classes in client code
- Drop commit versioning in `core` (which wasn't working anyway)

###  New
- Use `f2py_jit` to compile extensions at run time
- Add `Correlation.asarray()`
- Add `Correlation.results` to gather `grid`, `values`, `analysis`
- Add `Correlation.values` as replacement of `Correlation.value` (still supported)
- New naming scheme for `Correlation.analysis` (may be superseeded in the future)
- Return results and analysis in `Correlation.do()`
- Support pickling of `Correlation` and remove internal data at the end of `compute()`

###  Bug fixes
- Fix writing of some subclasses
- Clarify that minimum working python version is 3.7
- Replace `setup.py` with `pyproject.toml`

## 3.4.0 - 2024/01/25

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.3.3...3.4.0)

###  New
- Add automatic detection of species for partial correlations. No need to pass a species tuple to `Partial` anymore
- Add `**kwargs` to all `Correlation` subclasses. This fixes the issue that some arguments were not passed to the base class by some subclasses.
- Make `Partial` callable like `Correlation`
- Add `output_path` argument to `Correlation.write()` to write results to file even when the trajectory was in RAM
- Enable pickling of `Correlation` objects
- Import `api` in __init__.py

###  Bug fixes
- Improve interpolation of columns header in `Correlation.write()`:
  - Add custom `tag_subscript` variable to control tag formatting (example: `F_s^A(k,t)`)
  - Wrap tags in `{}` when longer than 1 (example: `F_s^{A-A}(k,t)`)
  Strictly speaking, these changes may break backward compatibility with client code that parsed columns header.
- Refactor a bit writing of metadata in `Correlation.write()`
- Fix writing relaxation time as `nan` when it is not found. Previous behavior was not to write anything. This prevented from reading the file, for instance, with `numpy.loadtxt`.
- Fix issues with `StructureFactorFast`

## 3.3.0 - 2023/08/03

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.2.0...3.3.0)

###  New
- Add `FourierSpaceGrid` to precompute the k-vectors grid. This enables some optimizations with partial correlations of correlation functions in Fourier space

###  Bug fixes
- Refactor `FourierSpaceCorrelation` via strategy pattern thanks to FourierSpaceGrid. The `kgrid` argument of correlation instances derived from `FourierSpaceCorrelation` can now be a `FourierSpaceGrid` instance, which allows one to specify grids with individual k-vectors and to share the same set of k-vectors between correlation instances.

## 3.2.0 - 2023/07/01

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.1.3...3.2.0)

###  New
- Add support for BondAngleDistribution with non-periodic cells

## 3.1.3 - 2023/03/11

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.1.2...3.1.3)

###  Bug fixes
- Fix `S4ktOverlap.write()` by correcting the grid layout (thanks to Jia Song)
- Fix Makefile coverage target not returning error code
- Fix error with `numpy.int`, for compatibility with numpy 1.24

## 3.1.2 - 2023/02/23

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.1.1...3.1.2)

###  Bug fixes
- Fix `S4ktOverlap`

## 3.1.1 - 2023/02/06

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.1.0...3.1.1)

###  Bug fixes
- Fix `numpy.complex` deprecation
- Fix handling non-periodic cells with linked cells and g(r) calculation
- Fix handling rmax with non-periodic boxes; disable linked cells with `rmax` because of normalization issue
- Fix memory access error in `gr_neighbors_distinct_c()`; this occurred when computing gr with fast kernel and linked cells (bug system or small rmax)

## 3.1.0 - 2022/12/10

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/3.0.2...3.1.0)

###  New
- Add `Correlation.show()` to display correlation function via matplotlib
- Add partial BA correlations in api
###  Bug fixes
- Fix updating partial correlations
- Fix behavior of `Correlation.write()` when trajectory.filename is `None`
- Fix CM fixing when reading trajectories with `position_unfolded` variables
- Fix linked cells in 2d
- Fix `Partial` not copying analysis dict for cross correlations
- Fix bond angle distribution, which assumed A-B-A and B-A-B

## 3.0.0 - 2021/10/21

[Full diff](https://framagit.org/atooms/postprocessing/-/compare/2.7.2...3.0.0)

### Changed
- Total and self intermediate scattering functions have the same time grid by default
- Privatize `FourierSpaceCorrelation.kvector` as `FourierSpaceCorrelation._kvectors`
- Adaptative bin width for `FourierSpaceCorrelation.kgrid`, it is automatically increased until all bins in `kgrid` are non-empty
- Rename some private variables in the `FourierSpaceCorrelation` hierarchy

### New
- Add `FourierSpaceCorrelation.kvectors` as a property to store the lists of k-vectors used for each entry in `FourierSpaceCorrelation.kgrid`
- Add `Partial.output_path` property
