PROJECT=atooms/postprocessing
PACKAGE=atooms.postprocessing

.PHONY: all test install docs coverage pep8 debug clean

all: install

docs:
	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/index.org -f org-rst-export-to-rst --kill
	# orgnb.py docs/*.org
	make -C docs/ html

test:
	python -m unittest discover -s tests

coverage: install
	coverage run --source $(PACKAGE) -m unittest discover -s tests; \
	code=$$?; \
	coverage report; \
	exit $$code

pep8:
	autopep8 -r -i $(PROJECT)
	autopep8 -r -i tests
	flake8 $(PROJECT)

clean:
	find $(PROJECT) tests -name '*.pyc' -name '*.so' -exec rm '{}' +
	find $(PROJECT) tests -name '__pycache__' -exec rm -r '{}' +
	rm -rf build/ dist/
