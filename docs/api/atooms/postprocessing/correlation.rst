atooms.postprocessing.correlation
==========================================

.. automodule:: atooms.postprocessing.correlation
   :members:
   :undoc-members:
   :special-members: __init__
