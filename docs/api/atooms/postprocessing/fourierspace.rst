atooms.postprocessing.fourierspace
==========================================

.. automodule:: atooms.postprocessing.fourierspace
   :members:
   :undoc-members:
   :special-members: __init__
