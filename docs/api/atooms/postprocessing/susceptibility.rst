atooms.postprocessing.susceptibility
==========================================

.. automodule:: atooms.postprocessing.susceptibility
   :members:
   :undoc-members:
   :special-members: __init__
