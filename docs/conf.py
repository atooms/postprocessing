# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../'))

# -- Project information -----------------------------------------------------

project = 'atooms-pp'
copyright = '2016-2025, Daniele Coslovich'
author = 'Daniele Coslovich'
repo = 'https://framagit.org/atooms/postprocessing'
links =  {'Public API': 'https://atooms.frama.io/postprocessing/api/postprocessing',
          'Notebooks @ Framagit': 'https://framagit.org/atooms/postprocessing/-/tree/master/docs'},

# -- General configuration ---------------------------------------------------

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.coverage', 'sphinx.ext.napoleon']

autodoc_class_signature = 'separated'
coverage_statistics_to_report = True
coverage_statistics_to_stdout = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The root document.
root_doc = 'index'

# Highlights: do not highlight literal blocks
highlight_language = 'none'
default_role = 'obj'

# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
pygments_style = 'lightbulb'
html_theme = 'furo'
html_static_path = ['_static/furo']
html_css_files = ['custom.css']
html_theme_options = {
    "light_css_variables": {
        # "code-font-size": "--var(--font-size--small)",  # does not work?
        "api-font-size": "--var(--font-size--normal)",
        "admonition-title-font-size": "1rem",
        "admonition-font-size": "1rem",
    },
}
