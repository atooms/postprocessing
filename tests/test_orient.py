#!/usr/bin/env python
import unittest
import numpy
from atooms.postprocessing.cl import RotationalCorrelation, C1Correlation, \
    C2Correlation, EndToEndCorrelation

# TODO: speed up these tests / optimize cl correlations
class Test(unittest.TestCase):

    def test_orient(self):
        import numpy
        from atooms.system import Particle, Molecule, System, Cell
        from atooms.trajectory import MolecularTrajectoryXYZ

        finp = '/tmp/test_molecular.xyz'
        molecule = Molecule([Particle(position=[1.0, 0.0], species=1),
                             Particle(position=[0.0, 0.0], species=2),
                             Particle(position=[0.0, 1.0], species=2),], bond=[0, 1])
        s = System(molecule=[Molecule([Particle(position=[1.0, 0.0], species=1),
                                       Particle(position=[0.0, 0.0], species=2),
                                       Particle(position=[0.0, 1.0], species=2)],
                                      bond=[0, 1]) for _ in range(10)],
                   cell=Cell([6.0, 6.0]))

        numpy.random.seed(1)
        with MolecularTrajectoryXYZ(finp, 'w') as th:
            for step in range(1000):
                th.write(s, step)
                for m in s.molecule:
                    theta = 0.5*(numpy.random.random() - 0.5)
                    m.rotate(theta)

        with MolecularTrajectoryXYZ(finp) as th:
            cf = EndToEndCorrelation(th)
            cf.compute()
            self.assertAlmostEqual(cf.value[0], 1.0)
            # cf.show()
            cf = C1Correlation(th)
            cf.compute()
            self.assertAlmostEqual(cf.value[0], 1.0)
            # cf.show()
            cf = C2Correlation(th)
            cf.compute()
            self.assertAlmostEqual(cf.value[0], 1.0)
            # cf.show()

    def test_orient_place(self):
        import numpy
        from atooms.system import Particle, Molecule, System, Cell
        from atooms.trajectory import MolecularTrajectoryXYZ

        finp = '/tmp/test_molecular.xyz'
        molecule = Molecule([Particle(position=[1.0, 0.0, 0.], species=1),
                             Particle(position=[0.0, 0.0, 0.], species=2),
                             Particle(position=[0.0, 1.0, 0.], species=2),], bond=[0, 1])
        s = System(molecule=[Molecule([Particle(position=[1.0, 0.0, 0.], species=1),
                                       Particle(position=[0.0, 0.0, 0.], species=2),
                                       Particle(position=[0.0, 1.0, 0.], species=2)],
                                      bond=[0, 1]) for _ in range(10)],
                   cell=Cell([6.0, 6.0, 6.0]))

        numpy.random.seed(1)
        with MolecularTrajectoryXYZ(finp, 'w') as th:
            for step in range(500):
                for m in s.molecule:
                    theta = 0.5*(numpy.random.random() - 0.5)
                    m.rotate(theta, axis=[0., 0., 1.])
                th.write(s, step)

        with MolecularTrajectoryXYZ(finp) as th:
            cf = RotationalCorrelation(th, custom_orientation=['2-3x2-1'])
            cf.compute()
            self.assertTrue(sum(numpy.array(cf.value) != 1.0) == 0)

    def tearDown(self):
        from atooms.core.utils import rmf
        rmf('/tmp/test_molecular.xyz')
